
<div id="fh5co-testimonial">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                <h2>Information</h2>
            </div>
        </div>

            <ul>

            <?php foreach ($posts as $post):
                echo '<li>';
             echo $this->Html->link($post['title'],
                 ['controller' => 'posts', 'action' => 'view', $post['id']]);
                echo '</li>';
                endforeach;
            ?>
            </ul>
    <div class="paginator">
            <ul class="pagination" style="text-align: center">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
        </div>
    </div>

</div>