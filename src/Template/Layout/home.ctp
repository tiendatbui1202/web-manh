<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Law &mdash; Free Website Template, Free HTML5 Template by freehtml5.co</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Website Template by FreeHTML5.co" />
    <meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
    <meta name="author" content="FreeHTML5.co" />

    <!--
    //////////////////////////////////////////////////////

    FREE HTML5 TEMPLATE
    DESIGNED & DEVELOPED by FreeHTML5.co

    Website: 		http://freehtml5.co/
    Email: 			info@freehtml5.co
    Twitter: 		http://twitter.com/fh5co
    Facebook: 		https://www.facebook.com/fh5co

    //////////////////////////////////////////////////////
     -->

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Animate.css -->
    <link rel="stylesheet" href="/manh/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="/manh/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="/manh/css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="/manh/css/magnific-popup.css">

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="/manh/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/manh/css/owl.theme.default.min.css">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="/manh/css/flexslider.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="/manh/css/style_law.css">

    <!-- Modernizr JS -->
    <script src="/manh/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="/manh/js/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="fh5co-loader"></div>

<div id="page">
    <nav class="fh5co-nav" role="navigation">
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="fh5co-logo"><?= $this->Html->link(' I.S.Lab' , ['controller'=>'Pages' , 'action'=>'display'])?></div>
                    </div>
                    <div class="col-xs-10 text-right menu-1">
                        <ul>
                            <li <?php echo $active = ($_SESSION['active'] == 'home'?'class="active"':''); ?>><?= $this->Html->link(' Home' , ['controller'=>'Pages' , 'action'=>'display'])?></li>
                            <li <?php echo $active = ($_SESSION['active'] == 'introduction'?'class="active"':''); ?>><?= $this->Html->link('Introduction' , ['controller'=>'Introductions'])?></li>
                            <li <?php echo $active = ($_SESSION['active'] == 'professor'?'class="active"':''); ?>><?= $this->Html->link('Professor' , ['controller'=>'Professors'])?></li>
                            <li <?php echo $active = ($_SESSION['active'] == 'peoples'?'class="active"':''); ?>><?= $this->Html->link('People' , ['controller'=>'Peoples'])?></li>
                            <li <?php echo $active = ($_SESSION['active'] == 'posts'?'class="active"':''); ?>><?= $this->Html->link('Information' , ['controller'=>'Posts', 'action'=>'index'])?></li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </nav>

    <?= $this->fetch('content') ?>

    <footer id="fh5co-footer" role="contentinfo">
        <div class="container">
            <div class="row row-pb-md">
                <div class="col-md-6 fh5co-widget">
                    <img width="300" height="72" src="http://cglab.snu.ac.kr/cglabwordpress/wp-content/uploads/2017/09/LOGO_CGIP-300x72.png" class="image wp-image-258  attachment-medium size-medium" alt="" style="max-width: 100%; height: auto;" srcset="http://cglab.snu.ac.kr/cglabwordpress/wp-content/uploads/2017/09/LOGO_CGIP-300x72.png 300w, http://cglab.snu.ac.kr/cglabwordpress/wp-content/uploads/2017/09/LOGO_CGIP.png 503w" sizes="(max-width: 300px) 100vw, 300px">
                    <img width="300" height="68" src="http://cglab.snu.ac.kr/cglabwordpress/wp-content/uploads/2017/09/KakaoTalk_20170927_233847785-300x68.png" class="image wp-image-260  attachment-medium size-medium" alt="" style="max-width: 100%; height: auto;" srcset="http://cglab.snu.ac.kr/cglabwordpress/wp-content/uploads/2017/09/KakaoTalk_20170927_233847785-300x68.png 300w, http://cglab.snu.ac.kr/cglabwordpress/wp-content/uploads/2017/09/KakaoTalk_20170927_233847785.png 602w" sizes="(max-width: 300px) 100vw, 300px">
                </div>

                <div class="col-md-6 col-md-push-1">
                    <h4 style="color: white">Contact</h4>
                    <ul class="fh5co-footer-links">
                        <li>Address : 320, New Engineering Building 302 School of Computer Science and Engineering, Seoul National University, Seoul, Korea
                        </li>
                        <li>Tel : +82 2 880 1860</li>
                    </ul>
                </div>
            </div>


        </div>
    </footer>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<!-- jQuery -->
<script src="/manh/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="/manh/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="/manh/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="/manh/js/jquery.waypoints.min.js"></script>
<!-- Stellar Parallax -->
<script src="/manh/js/jquery.stellar.min.js"></script>
<!-- Carousel -->
<script src="/manh/js/owl.carousel.min.js"></script>
<!-- Flexslider -->
<script src="/manh/js/jquery.flexslider-min.js"></script>
<!-- countTo -->
<script src="/manh/js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="/manh/js/jquery.magnific-popup.min.js"></script>
<script src="/manh/js/magnific-popup-options.js"></script>
<!-- Main -->
<script src="/manh/js/main.js"></script>

</body>
</html>