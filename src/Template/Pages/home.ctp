
<aside id="fh5co-hero" class="js-fullheight">
    <div class="flexslider js-fullheight">
        <ul class="slides">
            <li style="background-image: url(/manh/webroot/images/img_bg_1.jpg);">
                <div class="overlay-gradient"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
                            <div class="slider-text-inner">
                                <h1>Welcome to CGIP</h1>
                                <p><a class="btn btn-primary btn-lg" href="practice.html">Learn more</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li style="background-image: url(/manh/webroot/images/img_bg_2.jpg);">
                <div class="overlay-gradient"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
                            <div class="slider-text-inner">
                                <h1>Welcome to CGIP</h1>
                                <p><a class="btn btn-primary btn-lg btn-learn" href="practice.html">Learn more</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li style="background-image: url(/manh/webroot/images/img_bg_3.jpg);">
                <div class="overlay-gradient"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
                            <div class="slider-text-inner">
                                <h1>Welcome to CGIP</h1>

                                <p><a class="btn btn-primary btn-lg btn-learn" href="practice.html">Learn more</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</aside>

<div id="fh5co-counter" class="fh5co-counters fh5co-bg-section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center animate-box">
                <span class="icon"><i class="fa fa-futbol-o"></i></span>
                <h4 style="font-size: 18px; font-weight: bold">Volume visualization</h4>
                <p>Enhanced 3D volume Rendering and
                    High Quality Volume Rendering</p>
            </div>
            <div class="col-md-3 text-center animate-box">
                <span class="icon"><i class="fa fa-heartbeat"></i></span>
                <h4 style="font-size: 18px; font-weight: bold">Medical Image Processing</h4>
                <p>Medical object segmentation and classification
                </p>
            </div>
            <div class="col-md-3 text-center animate-box">
                <span class="icon"><i class="fa fa-files-o"></i></span>
                <h4 style="font-size: 18px; font-weight: bold">REGISTRATION
                </h4>
                <p>transforming different medical data into one format
                </p>
            </div>
            <div class="col-md-3 text-center animate-box">
                <span class="icon"><i class="fa fa-cubes"></i></span>
                <h4 style="font-size: 18px; font-weight: bold">GPU_Based Reconstruction
                </h4>
                <p>Reconstruction of 3D volumetric image from projection data
                </p>
            </div>
        </div>
    </div>
</div>