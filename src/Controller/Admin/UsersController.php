<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use function PHPSTORM_META\elementType;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AdminAppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if(!empty($this->request->getData('avatar')['name'])){
                $file = $this->request->getData('avatar');
                $file['name'] = $this->wp_modify_uploaded_file_names($data['avatar']['name']);

                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension

                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions

                if(in_array($ext, $arr_ext))
                {
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/users/' . $file['name']);
                    $data['avatar'] = $file['name'];
                }
            }
            $user = $this->Users->patchEntity($user, $data);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if(!empty($this->request->getData('new-avatar')['name'])){
                $file = $this->request->getData('new-avatar');
                if(file_exists(WWW_ROOT . 'img/users/' . $user['avatar'])){
                    unlink(WWW_ROOT . 'img/users/' . $user['avatar']);
                }
                $file['name'] = $this->wp_modify_uploaded_file_names($data['new-avatar']['name']);

                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension

                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions

                if(in_array($ext, $arr_ext))
                {
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/users/' . $file['name']);
                    $data['avatar'] = $file['name'];
                }
            }else{
                $data['avatar'] = $user['avatar'];
            }
            $user = $this->Users->patchEntity($user, $data);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($user['avatar'] != ''){
            unlink(WWW_ROOT . 'img/users/' . $user['avatar']);
        }

        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login(){
        $users = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $users = $this->Users->patchEntity($users, $this->request->getData());
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);

                $this->Flash->success('Logged in');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('login fail !');
            }
        }
    }

    public function  logout()
    {
        $this->Flash->success('You are logged out');
        $this->request->session()->destroy();
        return $this->redirect($this->Auth->logout());
    }

}
