<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Introduction $introduction
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Introductions'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="introductions form large-9 medium-8 columns content">
    <?= $this->Form->create($introduction) ?>
    <fieldset>
        <legend><?= __('Add Introduction') ?></legend>
        <?php
            echo $this->Form->control('content');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
